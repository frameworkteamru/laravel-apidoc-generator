# Info

@if ($customInfoText)
{!! $customInfoText !!}
@else
Welcome to the generated API reference.
@endif

@if($showPostmanCollectionButton)
[Get Postman Collection]({{url($outputPath.'/collection.json')}})
@endif