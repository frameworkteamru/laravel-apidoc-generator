<?php

namespace Mpociot\ApiDoc\Postman;

use Ramsey\Uuid\Uuid;
use Illuminate\Support\Collection;

class CollectionWriter
{
    /**
     * @var Collection
     */
    private $routeGroups;

    /**
     * CollectionWriter constructor.
     *
     * @param Collection $routeGroups
     */
    public function __construct(Collection $routeGroups)
    {
        $this->routeGroups = $routeGroups;
    }

    public function getCollection()
    {
        $collection = [
            'variables' => [],
            'info' => [
                'name' => '',
                '_postman_id' => Uuid::uuid4()->toString(),
                'description' => '',
                'schema' => 'https://schema.getpostman.com/json/collection/v2.0.0/collection.json',
            ],
            'item' => $this->routeGroups->map(function ($routes, $groupName) {
                return [
                    'name' => $groupName,
                    'description' => '',
                    'item' => $routes->map(function ($route) {
                        return [
                            'name' => $route['title'] != '' ? $route['title'] : url($route['uri']),
                            'request' => [
                                'url' => url($route['uri']),
                                'method' => $route['methods'][0],
                                'body' => [
                                    'mode' => 'formdata',
                                    'formdata' => collect($route['parameters'])
                                        ->filter(function ($value, $key){
                                            if ($value['type'] == 'array') {
                                                return false;
                                            }
                                            return true;
                                        })
                                        ->map(function ($parameter, $key) {
                                            return [
                                                'key' => $this->dotNotationToBrackets($key),
                                                'value' => isset($parameter['value']) ? $parameter['value'] : '',
                                                'type' => 'text',
                                                'enabled' => true,
                                            ];
                                         })->values()->toArray(),
                                ],
                                'description' => $route['description'],
                                'response' => [],
                            ],
                        ];
                    })->toArray(),
                ];
            })->values()->toArray(),
        ];

        return json_encode($collection);
    }

    public static function dotNotationToBrackets($string)
    {
        // replace not last indexes
        // array.index1.index2.index3 => array[index1][index2].index3
        $pattern = '/\.(.+?)\./i';
        $replacement = '[${1}].';

        for ($i = 0; $i <= substr_count($string, '.'); $i++) {
            $string = preg_replace($pattern, $replacement, $string, 1);
        }

        // replace last index
        // array[index1][index2].index3 => array[index1][index2][index3]
        $pattern = '/\.(.+)/i';
        $replacement = '[${1}]';
        $string = preg_replace($pattern, $replacement, $string, 1);

        return $string;
    }
}
